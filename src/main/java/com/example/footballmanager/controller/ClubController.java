package com.example.footballmanager.controller;

import com.example.footballmanager.entity.ClubEntity;
import com.example.footballmanager.repository.ClubRepository;
import com.example.footballmanager.service.ClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Contrôleur gérant les opérations liées aux clubs de football.
 */
@RestController
@RequestMapping("/club")
public class ClubController {

    @Autowired
    private final ClubRepository clubRepository;

    @Autowired
    private final ClubService clubService;

    /**
     * Constructeur du contrôleur avec injection de dépendances.
     *
     * @param clubRepository Le repository des clubs.
     * @param clubService    Le service des clubs.
     */
    public ClubController(ClubRepository clubRepository, ClubService clubService) {
        this.clubRepository = clubRepository;
        this.clubService = clubService;
    }

    /**
     * Récupère tous les clubs.
     *
     * @return La liste de tous les clubs.
     */
    @GetMapping("/all")
    public List<ClubEntity> getClub() {
        return clubRepository.findAll();
    }

    /**
     * Récupère un club par son identifiant.
     *
     * @param id L'identifiant du club.
     * @return Le club correspondant à l'identifiant.
     */
    @GetMapping("/{id}")
    public Optional<ClubEntity> getClubById(@PathVariable Long id) {
        return clubRepository.findById(id);
    }

    /**
     * Ajoute un nouveau club.
     *
     * @param club Le club à ajouter.
     * @return Réponse HTTP indiquant le succès de l'opération.
     */
    @PostMapping("/add")
    public ResponseEntity<String> addClub(@RequestBody ClubEntity club) {
        // Sauvegarde du club dans le repository
        club = this.clubRepository.save(club);
        return new ResponseEntity<>("Club ajouté avec succès", HttpStatus.CREATED);
    }

    /**
     * Met à jour un club existant.
     *
     * @param id   L'identifiant du club à mettre à jour.
     * @param club Les nouvelles données du club.
     * @return Le club mis à jour.
     */
    @PutMapping("/update/{id}")
    public ClubEntity updateClub(@PathVariable Long id, @RequestBody ClubEntity club) {
        // Attribution de l'identifiant au club avant la mise à jour
        club.setId(id);
        // Sauvegarde du club mis à jour dans le repository
        return this.clubRepository.save(club);
    }

    /**
     * Supprime un club par son identifiant.
     *
     * @param id L'identifiant du club à supprimer.
     * @return Réponse HTTP indiquant le succès ou l'échec de l'opération.
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteClub(@PathVariable Long id) {
        // Appel au service pour effectuer la suppression du club
        return clubService.clubDelete(id);
    }
}
