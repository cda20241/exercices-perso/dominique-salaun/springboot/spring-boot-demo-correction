package com.example.footballmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.footballmanager.repository.JoueurRepository;
import com.example.footballmanager.entity.JoueurEntity;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/joueur")
public class JoueurController {

    @Autowired
    private final JoueurRepository joueurRepository;

    // Injection de dépendance via le constructeur
    public JoueurController(JoueurRepository joueurRepository) {
        this.joueurRepository = joueurRepository;
    }

    /**
     * Récupère tous les joueurs de la base de données.
     *
     * @return Liste des joueurs.
     */
    @GetMapping("/all")
    public List<JoueurEntity> getAllJoueurs() {
        return joueurRepository.findAll();
    }

    /**
     * Récupère un joueur en fonction de son identifiant.
     *
     * @param id Identifiant du joueur.
     * @return Une instance de Joueur ou null si non trouvé.
     */
    @GetMapping("/{id}")
    public JoueurEntity getJoueur(@PathVariable Long id) {
        Optional<JoueurEntity> joueur = joueurRepository.findById(id);
        return joueur.orElse(null);
    }

    /**
     * Ajoute un nouveau joueur à la base de données.
     *
     * @param joueur Nouveau joueur à ajouter.
     * @return Le joueur ajouté avec l'ID généré.
     */
    @PostMapping("")
    public JoueurEntity addJoueur(@RequestBody JoueurEntity joueur) {
        // On set l'id à null pour éviter de remplacer un joueur existant (pas une mise à jour ici)
        joueur.setId(null);
        joueur = joueurRepository.save(joueur);
        return joueur;
    }

    /**
     * Modifie un joueur existant en fonction de son identifiant.
     *
     * @param id     Identifiant du joueur à modifier.
     * @param joueur Nouvelles données du joueur.
     * @return Une instance de Joueur modifiée.
     */
    @PutMapping("/{id}")
    public JoueurEntity updateJoueur(@PathVariable Long id, @RequestBody JoueurEntity joueur) {
        return joueurRepository.save(joueur);
    }

    /**
     * Supprime un joueur en fonction de son identifiant.
     *
     * @param id Identifiant du joueur à supprimer.
     * @return ResponseEntity avec le statut correspondant.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteJoueur(@PathVariable Long id) {
        Optional<JoueurEntity> joueur = joueurRepository.findById(id);
        if (joueur.isPresent()) {
            joueurRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
