package com.example.footballmanager.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Entité représentant un club de football.
 */
@Entity
@Table(name = "club")
public class ClubEntity {

    // Identifiant unique du club généré automatiquement
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_club", nullable = false, updatable = false)
    private Long id;

    // Nom du club
    @Column(name = "nomClub")
    private String nom;

    // Ville où est basé le club
    @Column
    private String ville;

    // Relation One-to-Many avec JoueurEntity
    @OneToMany(mappedBy = "club", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<JoueurEntity> joueurEntities = new ArrayList<>();

    /**
     * Constructeur par défaut.
     */
    public ClubEntity() {
    }

    /**
     * Constructeur avec les données de base.
     *
     * @param nom             Le nom du club.
     * @param ville           La ville du club.
     * @param joueurEntities La liste des joueurs du club.
     */
    public ClubEntity(String nom, String ville, List<JoueurEntity> joueurEntities) {
        this.nom = nom;
        this.ville = ville;
        this.joueurEntities = joueurEntities;
    }

    /**
     * Constructeur avec l'identifiant, le nom, la ville et la liste des joueurs du club.
     *
     * @param id              L'identifiant du club.
     * @param nom             Le nom du club.
     * @param ville           La ville du club.
     * @param joueurEntities La liste des joueurs du club.
     */
    public ClubEntity(Long id, String nom, String ville, List<JoueurEntity> joueurEntities) {
        this.id = id;
        this.nom = nom;
        this.ville = ville;
        this.joueurEntities = joueurEntities;
    }

    /**
     * Obtient la liste des joueurs du club.
     *
     * @return La liste des joueurs du club.
     */
    public List<JoueurEntity> getJoueurEntities() {
        return joueurEntities;
    }

    /**
     * Définit la liste des joueurs du club.
     *
     * @param joueurEntities La nouvelle liste des joueurs du club.
     */
    public void setJoueurEntities(List<JoueurEntity> joueurEntities) {
        this.joueurEntities = joueurEntities;
    }

    /**
     * Obtient l'identifiant du club.
     *
     * @return L'identifiant du club.
     */
    public Long getId() {
        return id;
    }

    /**
     * Définit l'identifiant du club.
     *
     * @param id Le nouvel identifiant du club.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Obtient le nom du club.
     *
     * @return Le nom du club.
     */
    public String getNom() {
        return nom;
    }

    /**
     * Définit le nom du club.
     *
     * @param nom Le nouveau nom du club.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Obtient la ville du club.
     *
     * @return La ville du club.
     */
    public String getVille() {
        return ville;
    }

    /**
     * Définit la ville du club.
     *
     * @param ville La nouvelle ville du club.
     */
    public void setVille(String ville) {
        this.ville = ville;
    }
}
