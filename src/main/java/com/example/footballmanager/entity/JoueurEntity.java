package com.example.footballmanager.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

/**
 * Entité représentant un joueur de football.
 */
@Entity
@Table(name="joueur")
public class JoueurEntity {

    // Identifiant unique du joueur généré automatiquement
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_joueur", nullable = false, updatable = false)
    private Long id;

    // Nom du joueur
    @Column
    private String nom;

    // Prénom du joueur avec une longueur maximale de 60 caractères
    @Column(length = 60)
    private  String prenom;

    // Relation Many-to-One avec ClubEntity
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_club")
    @JsonBackReference
    private ClubEntity club;

    /*********************** CONSTRUCTEUR *********************/

    // Constructeur par défaut
    public JoueurEntity() {
    }

    /**
     * Constructeur avec les données de base.
     *
     * @param nom   Le nom du joueur.
     * @param prenom Le prénom du joueur.
     * @param club   Le club auquel le joueur est associé.
     */
    public JoueurEntity(String nom, String prenom, ClubEntity club) {
        this.nom = nom;
        this.prenom = prenom;
        this.club = club;
    }

    /**
     * Constructeur avec l'identifiant, le nom, le prénom et le club du joueur.
     *
     * @param id     L'identifiant du joueur.
     * @param nom    Le nom du joueur.
     * @param prenom Le prénom du joueur.
     * @param club   Le club auquel le joueur est associé.
     */
    public JoueurEntity(Long id, String nom, String prenom, ClubEntity club) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.club = club;
    }

    /*********************** GETTER et SETTER *********************/

    // Les méthodes Getter et Setter pour accéder et modifier les attributs de la classe.

    /**
     * Obtient l'identifiant du joueur.
     *
     * @return L'identifiant du joueur.
     */
    public Long getId() {
        return id;
    }

    /**
     * Obtient le nom du joueur.
     *
     * @return Le nom du joueur.
     */
    public String getNom() {
        return nom;
    }

    /**
     * Obtient le prénom du joueur.
     *
     * @return Le prénom du joueur.
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Définit l'identifiant du joueur.
     *
     * @param id Le nouvel identifiant du joueur.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Définit le nom du joueur.
     *
     * @param nom Le nouveau nom du joueur.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Définit le prénom du joueur.
     *
     * @param prenom Le nouveau prénom du joueur.
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Obtient le club auquel le joueur est associé.
     *
     * @return Le club du joueur.
     */
    public ClubEntity getClub() {
        return club;
    }

    /**
     * Définit le club auquel le joueur est associé.
     *
     * @param club Le nouveau club du joueur.
     */
    public void setClub(ClubEntity club) {
        this.club = club;
    }
}
