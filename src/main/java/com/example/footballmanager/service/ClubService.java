package com.example.footballmanager.service;

import com.example.footballmanager.entity.ClubEntity;
import com.example.footballmanager.repository.ClubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClubService{
    @Autowired
    private final ClubRepository clubRepository;

    public ClubService(ClubRepository clubRepository){
        this.clubRepository = clubRepository;
    }

    public ResponseEntity<String> clubDelete(Long id){
        Optional<ClubEntity> club = this.clubRepository.findById(id);
        if (club.isPresent()) {
            this.clubRepository.deleteById(id);
            return new ResponseEntity<>("Le club est supprimer", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Club non trouve", HttpStatus.NOT_FOUND);
        }

    }

}
