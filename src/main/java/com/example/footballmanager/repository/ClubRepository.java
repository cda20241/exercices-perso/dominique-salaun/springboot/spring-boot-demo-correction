package com.example.footballmanager.repository;

import com.example.footballmanager.entity.ClubEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface Repository pour la gestion des données des clubs de football.
 */
public interface ClubRepository extends JpaRepository<ClubEntity, Long> {

    // La classe ClubRepository hérite de JpaRepository
    // JpaRepository fournit déjà toutes les fonctions les plus courantes de requêtes à la base de données
    // La fonction findById(), findAll(), save(), etc.
    // En général, on n'a rien de plus à ajouter dans le repository, car les opérations CRUD sont couvertes par JpaRepository.
    // Chaque repository doit être associé à une classe d'entité spécifique.

    // Si nécessaire, vous pouvez ajouter des méthodes spécifiques de requête ici,
    // par exemple, findByNom(String nom) pour rechercher un club par son nom.
    // Ces méthodes seront automatiquement implémentées par Spring Data JPA.
}
